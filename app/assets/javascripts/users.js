var ready = function(){
	$("#create_service_link").on('click',function(e){
		e.preventDefault();
		$("#create_service_modal").modal({
		show: true
		});	
	});

	$(".new-appointment").on('click', function(e){
		$("#create_appointment_modal").modal({
			show: true
		});
		console.log(this);
		$('#appointment_service_id').val(this.id.split('_')[2]);
		$('#appointment_doctor_id').val(this.id.split('_')[3]);
	});
};

$(document).ready(ready);
$(document).on('page:load',ready);