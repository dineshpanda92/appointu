class UsersController < ApplicationController

	before_action :is_user_logged_in?, only: [:show]

  def new
  	@user = User.new
  end

  def create
  	@user = User.new(user_params)
  	if @user.save
  		flash[:success] = "Welcome to AppointU!"
  		login user=@user
  		current_user
  		redirect_to @user
  	else
  		render 'new'
  	end
  end

  def show
    if current_user.user_type == "doctor"
      @services = current_user.services.order("created_at DESC")
    else
      @services = Service.order("title ASC")
    end
  	@user = current_user
  	@service = Service.new
  end

  private
  def user_params
  	params.require(:user).permit(:name,:email,:password,:password_confirmation,:user_type)
  end
end
