class AppointmentsController < ApplicationController

	respond_to :js, :html

  def create
  	start_time = Time.new(params[:appointment]["start_time(1i)"].to_i,
  						  params[:appointment]["start_time(2i)"].to_i,
  						  params[:appointment]["start_time(3i)"].to_i,
  						  params[:appointment]["start_time(4i)"].to_i,
  						  params[:appointment]["start_time(5i)"].to_i)
  	@appointment = current_user.appointments.new(:start_time => start_time,
  											    :service_id => params[:appointment]["service_id"],
                            :doctor_id  => params[:appointment]["doctor_id"])
    if validate_appointment(appointment = @appointment)
      render js: "alert('Invalid Date or Time Seletion. Make sure your appointment time is greater than the current time.');"
    elsif @appointment.check_doctor_schedule
      render js: "alert('This time slot is already booked for the doctor.Try in some other time slot.');"
    else
  	if @appointment.save
  		respond_to do|format|
  			format.js
  		end
  	end
  end
  end

  def index
    @appointments = Appointment.where(status: "pending", doctor_id: current_user.id)
  end

  def update
    @appointment = Appointment.find_by_id(params[:id])
    @appointment.update_attributes(status: "confirmed")
  end

  def destroy
    @appointment = Appointment.find_by_id(params[:id])
    @appointment.destroy
  end

end
