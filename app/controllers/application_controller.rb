class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  include SessionsHelper
  include AppointmentsHelper

  def is_user_logged_in?
  	if current_user.nil?
  		flash[:warning] = "Please login first!"
  		redirect_to login_url
  	end
  end

end
