class SessionsController < ApplicationController

	def new
	end

	def create
		user = User.find_by_email(params[:session][:email].downcase)
		if user && user.authenticate(params[:session][:password])
			login user
			current_user
			redirect_to user
		else
			flash[:warning] = "Your email and password do not match!"
			render 'new'
		end
	end

	def destroy
		logout
		redirect_to root_url
	end

end
