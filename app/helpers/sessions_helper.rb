module SessionsHelper

	def login(user)
		session[:user_id] = user.id
	end

	def current_user
		@curr_user ||= User.find_by_id(session[:user_id])
	end

	def logout
		@curr_user = nil
		session[:user_id] = nil
	end

end
