module AppointmentsHelper

	def doctor_appointment_count
		Appointment.where(status: "pending", doctor_id: current_user.id).count
	end

	#Get Date and Time (IST) from start_time of Appointment
	def get_date(time)
		time.to_date
	end

	def get_ist(time)
		time.in_time_zone("Kolkata").strftime("%T %p")
	end

	#To validate an appointment while creating it
	def validate_appointment(appointment)
		DateTime.now > appointment.start_time
	end

end
