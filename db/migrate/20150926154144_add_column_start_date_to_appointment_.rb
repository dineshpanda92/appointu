class AddColumnStartDateToAppointment < ActiveRecord::Migration
  def change
  	add_column :appointments, :start_date, :datetime
  end
end
