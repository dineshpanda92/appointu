class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :title
      t.text :desc
      t.decimal :price, default: 0.00, precision: 2, scale: 2
      t.timestamps null: false
    end
  end
end
